FROM php:7.2

RUN apt-get update && \
    apt-get install -y zlib1g-dev libxml2-dev

RUN pecl install xdebug
RUN docker-php-ext-install xml zip
RUN docker-php-ext-enable xdebug

WORKDIR /data
COPY . /data

RUN php composer.phar install --optimize-autoloader