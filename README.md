Symfony Git Bundle
=========================

[![Latest Version on Packagist][icon-version]][link-packagist]
[![Downloads on Packagist][icon-downloads]][link-packagist]
[![Software License][icon-license]](LICENSE)

### Install it with Composer

    composer require --dev jbtcd/git-bundle
    

### Note
This plugin makes Symfony significantly slower. Approximately 10 ms per commit to be read. By default, this means about 100 ms.

## License
The MIT License (MIT). Please see [License File](LICENSE) for more information.

[icon-version]: https://img.shields.io/packagist/v/jbtcd/git-bundle.svg?style=flat-square
[icon-downloads]: https://img.shields.io/packagist/dt/jbtcd/git-bundle.svg?style=flat-square
[icon-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/jbtcd/git-bundle
